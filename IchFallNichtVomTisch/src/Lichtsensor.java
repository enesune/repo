import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;

public class Lichtsensor {
	LightSensor forceOfLight = new LightSensor(SensorPort.S2);
	
	public Lichtsensor() {
		forceOfLight.calibrateLow();
		forceOfLight.setFloodlight(true);
		forceOfLight.calibrateHigh();
	}
	
	public void izzTisch() throws LightException {
		int lV = forceOfLight.readNormalizedValue();
		LCD.clear();
		LCD.drawInt(lV, 0, 0);
		LCD.refresh();
		if (lV > 400) return;
		throw new LightException();
	}
}
