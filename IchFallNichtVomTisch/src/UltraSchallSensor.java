import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

public class UltraSchallSensor {
	UltrasonicSensor nonPlusUltra= new UltrasonicSensor(SensorPort.S3);
	int distance= nonPlusUltra.getDistance();
	
	
	public void izzTisch() throws UltraException {
		int dist = nonPlusUltra.getDistance();
		LCD.clear();
		LCD.drawInt(dist, 0, 1);
		LCD.refresh();
		if (dist < 14) return;
		throw new UltraException();
	}

}
