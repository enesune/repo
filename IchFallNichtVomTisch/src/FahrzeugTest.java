
/**
   description Testklasse zur Demonstration der N�tzlichkeit von
               Interfaces

  @author      Ehlert / Hafezi / G�kcen / Trutz OSZ IMT
  @version     1.0 / 08.10.2008
  @version     1.1 / 22.01.2013
  @version     1.2 / 09.01.2019  
*/
import java.lang.System;
import lejos.nxt.*;

public class FahrzeugTest {

	// Anfang Attribute

	// Ende Attribute

	// Anfang Methoden
	public static void main(String[] args) {
		

		// Speed-Default-Wert ist 360
		IFahrzeugInterface einFahrzeug = new Fahrzeug_old();
		// Fahrzeugnamen �ndern

		// 1. Aufgabe
		Button.waitForAnyPress();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for (int i = 1; i <= 2; i++) {
			einFahrzeug.fahreVorwaertsStrecke(50);
			einFahrzeug.dreheRechtsGrad(90);
			einFahrzeug.fahreVorwaertsStrecke(20);
			einFahrzeug.dreheRechtsGrad(90);
		}

		// 2. Aufgabe
		for (int i = 1; i <= 36; i++) {
			einFahrzeug.fahreVorwaertsStrecke(5);
			einFahrzeug.dreheLinksGrad(10);
		}
		
		// 3. Aufgabe
		
		einFahrzeug.setGeschwindigkeit(600, 600);
		for (int i = 1; i <= 2; i++) {
			einFahrzeug.fahreVorwaertsStrecke(50);
			einFahrzeug.dreheRechtsGrad(90);
			einFahrzeug.fahreVorwaertsStrecke(20);
			einFahrzeug.dreheRechtsGrad(90);
		}

		// 4. Aufgabe
		// Fahrzeug f�hrt 10s vorw�rts, dabei "beschleunigt" es,
		// anschlie�end bleibt es 2s stehen und dann f�hrt es mit
		// kleinster Geschwindigkeit 5s r�ckw�rts
		/*
		 * einFahrzeug.fahreVorwaertsMitAcceleration(); try { Thread.sleep(10000); }
		 * catch (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 **/

		einFahrzeug.setGeschwindigkeit(10, 10);
		einFahrzeug.fahreVorwaerts();
		long startMillis = System.currentTimeMillis();
		Sound.twoBeeps();
		for (int i = 10; System.currentTimeMillis() < startMillis + 10000; i++) {
			einFahrzeug.setGeschwindigkeit(i + 1, i - 1);
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		einFahrzeug.setGeschwindigkeit(10, 10);
		Sound.beep();
		einFahrzeug.fahreRueckwaerts();
		try {
			Thread.sleep(5000);
			Sound.twoBeeps();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	// Ende Methoden

}
