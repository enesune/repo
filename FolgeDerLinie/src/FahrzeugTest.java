import lejos.nxt.Button;

public class FahrzeugTest {
	static IFahrzeugInterface fahrzeug = new Fahrzeug(261, 259);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Button.ENTER.waitForPressAndRelease();
		Lichtsensor lSens = new Lichtsensor();
		//fahrzeug.
		boolean warWeiss = false;
		boolean warSchwarz = false;
		//while(true) {
		//			if (lSens.izzSchwarz()) {
		//				if (warWeiss) {fahrzeug.stoppe(); lSens.izzSchwarz();}
		//				while (warWeiss && !lSens.izzSchwarz()) {fahrzeug.dreheRechtsGrad(1);}
		//				fahrzeug.setGeschwindigkeit(401, 399);
		//				warWeiss = false;
		//				fahrzeug.fahreVorwaerts();
		//				warSchwarz = true;
		//			} else {
		//				if (warSchwarz) {fahrzeug.stoppe(); warSchwarz = false; fahrzeug.dreheRechtsGrad(3);} else {
		//					fahrzeug.setGeschwindigkeit(281, 279);
		//					fahrzeug.dreheLinks();
		//					warWeiss = true;
		//				}
		//			}
		//}
		boolean lastLeft = true;
		while (true) {
			if (lSens.izzSchwarz()) {
				fahrzeug.setGeschwindigkeit(800, 798);
				fahrzeug.fahreVorwaerts();
			} else {
				fahrzeug.setGeschwindigkeit(251, 149);
				lastLeft = findLine(lSens, lastLeft);
			}
		}
	}
	
	public static boolean findLine(Lichtsensor lSens, boolean turnLeft) {
		boolean isLine = false;
		long elapsetTime = -1;
		int turnTime = 10;
		///boolean turnLeft = true;
		fahrzeug.stoppe();
		sleep(150);
		long startTime = java.lang.System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			lSens.izzSchwarz();
		}
		if (lSens.izzSchwarz()) {
			return turnLeft;
		}
		
		while (!isLine) {
			if (elapsetTime == -1) {
				startTime = java.lang.System.currentTimeMillis();
				turnLeftOrRight(turnLeft);
				elapsetTime = 0;
			} else {
				elapsetTime = java.lang.System.currentTimeMillis() - startTime;
				if (lSens.izzSchwarz() && lSens.izzSchwarz()) {
					fahrzeug.stoppe();
					//if (lSens.izzSchwarz()) {
					isLine = true;
					//} else {
					//	turnLeftOrRight(turnLeft);
					//}
				} else {
					if (elapsetTime > turnTime) {
						//fahrzeug.stoppe();
					
						//fahrzeug.stoppe();
						turnLeft = !turnLeft;
						elapsetTime = -1;
						turnTime *= 2;
					}
				}
			}
		}
		for (int i = 0; i < 10; i++) {
			lSens.izzSchwarz();
		}
		if (!lSens.izzSchwarz()) {
			int korrTimes = 0;
			while (!lSens.izzSchwarz()) {
				if (turnLeft) {
					fahrzeug.dreheRechtsGrad(1);
				} else {
					fahrzeug.dreheLinksGrad(1);
				}
				korrTimes++;
				if (korrTimes > 10) {
					return findLine(lSens, !turnLeft);
				}
			}
			if (turnLeft) {
				fahrzeug.dreheRechtsGrad(1);
			} else {
				fahrzeug.dreheLinksGrad(1);
			}
			for (int i = 0; i < 10; i++) {
				lSens.izzSchwarz();
			}
		}
		return turnLeft;
	}
	
	public static void turnLeftOrRight(boolean turnLeft) {
		if (turnLeft) {
			fahrzeug.dreheLinks();
		} else {
			fahrzeug.dreheRechts();
		}		
	}
	
	public static void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

}
