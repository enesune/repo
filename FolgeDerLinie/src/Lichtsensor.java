import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;

public class Lichtsensor {
	LightSensor forceOfLight = new LightSensor(SensorPort.S2);
	
	public Lichtsensor() {
		forceOfLight.calibrateLow();
		forceOfLight.setFloodlight(true);
		forceOfLight.calibrateHigh();
	}
	
	public boolean izzSchwarz() {
		int lV = forceOfLight.readNormalizedValue();
		lV = lV / 2 + forceOfLight.readNormalizedValue() / 2;
		LCD.clear();
		LCD.drawInt(lV, 0, 0);
		LCD.refresh();
		if (lV > 450) return false;
		return true;
	}
}
