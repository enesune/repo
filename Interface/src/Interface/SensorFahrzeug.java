package Interface;
import lejos.nxt.*;

public class SensorFahrzeug extends Fahrzeug{
	private int schwarzerWert;
	private int weisserWert;
	public SensorPort lichtSensorPort = SensorPort.S2;
	public LightSensor forceOfLight= new LightSensor(lichtSensorPort);
	
	public void kalibriereWerte() {
	//	lichtSensor.
		
	}
	
	public void isSchwarzerBereich(){
	
	}
	
	public void isWeisserBereich(){
		
	}
	
	public void lichtSensorEin(){
		
	}
	
	public void lichtSensorAus(){
		
	}
	
	public int getSchwarzerWert() {
		return schwarzerWert;
	}
	public void setSchwarzerWert(int schwarzerWert) {
		this.schwarzerWert = schwarzerWert;
	}
	public int getWeisserWert() {
		return weisserWert;
	}
	public void setWeisserWert(int weisserWert) {
		this.weisserWert = weisserWert;
	}	
}
