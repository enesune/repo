package Interface;
import IFahrzeugInterface;
import lejos.nxt.*;

public class Fahrzeug implements IFahrzeugInterface, IAnzeigenInterface {
	public NXTRegulatedMotor rechterMotor = Motor.B;
	public NXTRegulatedMotor linkerMotor = Motor.C;
	final float DURCHMESSER_REIFEN = 5.6f;
	final float ABSTAND_ACHE = 11.1f;
	final int EINS_METER = 5715;
	final int geradeausR = 359;
	final int geradeausL = 361;

	public void setRechterMotor(NXTRegulatedMotor rechterMotor) {
		this.rechterMotor = rechterMotor;
	}

	public void setLinkerMotor(NXTRegulatedMotor linkerMotor) {
		this.linkerMotor = linkerMotor;
	}

	public void setGeschwindigkeit(int links, int rechts) {
		rechterMotor.setSpeed(rechts);
		linkerMotor.setSpeed(links);
	}

	Fahrzeug() {
		rechterMotor.setAcceleration(6000);
		linkerMotor.setAcceleration(6000);
		rechterMotor.setSpeed(geradeausR);
		linkerMotor.setSpeed(geradeausL);

	}

	Fahrzeug(int geschwindigkeitLinks, int geschwindigkeitRechts) {
		rechterMotor.setSpeed(geschwindigkeitRechts);
		linkerMotor.setSpeed(geschwindigkeitLinks);
		rechterMotor.setAcceleration(12000);
		linkerMotor.setAcceleration(12000);
	}

	public void fahreVorwaerts() {
		rechterMotor.forward();
		linkerMotor.forward();
	}

	public void fahreVorwaertsMitAcceleration() {

		rechterMotor.setAcceleration(100);
		linkerMotor.setAcceleration(100);
		fahreVorwaerts();
	}

	public void fahreVorwaertsStrecke(int streckeInCM) {
		fahreVorwaerts();
		try {
			Thread.sleep(EINS_METER / 100 * streckeInCM /** 360 / getLinkerMotor()*/);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void fahreVorwaertsZeit(long zeitInSek) {
		fahreVorwaerts();
		try {
			Thread.sleep(zeitInSek*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void fahreRueckwaerts() {
		rechterMotor.backward();
		linkerMotor.backward();
	}

	public void fahreRueckwaertsStrecke(int streckeInCM) {
		fahreRueckwaerts();
		try {
			Thread.sleep((EINS_METER / 100) * streckeInCM * 360 / getMotorSpeed());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void fahreRueckwaertsZeit(long zeitInSek) {
		fahreRueckwaerts();
		try {
			Thread.sleep(1000 * zeitInSek);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void dreheRechts() {
		rechterMotor.backward();
		linkerMotor.forward();
	}

	public void dreheRechtsGrad(int winkel) {
		rechterMotor.rotate((int) -(winkel*4.171)/2, true);
		linkerMotor.rotate((int) (winkel*4.171)/2,false);

/*		try {
			Thread.sleep((long) (5.1 * winkel * 360 / getLinkerMotor()));

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();*/
	}

	public void dreheRechtsZeit(long zeitInSek) {
		dreheRechts();
		try {
			Thread.sleep(1000 * zeitInSek);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void dreheLinks() {
		rechterMotor.forward();
		linkerMotor.backward();
	}

	public void dreheLinksGrad(int winkel) {
		
		rechterMotor.rotate((int) (winkel*4.171)/2, true);
		linkerMotor.rotate((int) -(winkel*4.171)/2,false);
		/*try {
			Thread.sleep((long) (5.1375 * winkel * 360 / getLinkerMotor()));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//stoppe();
	}

	public void dreheLinksZeit(long zeitInSek) {
		dreheLinks();
		try {
			Thread.sleep(1000 * zeitInSek);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void stoppe() {
		rechterMotor.stop(true);
		linkerMotor.stop();
	}

	public void zeigeText(String text) {
		LCD.clear();
		LCD.drawString(text, 1, 1);
	}

	public void zeigeZahl(float zahl) {
		LCD.clear();
		LCD.drawString(String.valueOf(zahl), 1, 1);
	}

	public int getRechterMotor() {
		return rechterMotor.getSpeed();
	}

	public int getLinkerMotor() {
		return linkerMotor.getSpeed();
	}

	public int getMotorSpeed() {
		return ((getRechterMotor() + getLinkerMotor()) / 2);
	}

	public float getDURCHMESSER_REIFEN() {
		return DURCHMESSER_REIFEN;
	}

	public float getABSTAND_ACHE() {
		return ABSTAND_ACHE;
	}
}