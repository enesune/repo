package Interface;


public interface IFahrzeugInterface {
	
	public void fahreVorwaertsMitAcceleration();
	public float getDURCHMESSER_REIFEN();
	public float getABSTAND_ACHE();
	public int getLinkerMotor();
	public int getRechterMotor();
	public int getMotorSpeed();
	public void setGeschwindigkeit(int links, int rechts);
	public void fahreVorwaerts();	 
	public void fahreVorwaertsStrecke(int streckeInMetern);
	public void fahreVorwaertsZeit(long zeitInSek);
	public void fahreRueckwaerts();	
	public void fahreRueckwaertsStrecke(int streckeInMetern);
	public void fahreRueckwaertsZeit(long zeitInSek);
	public void dreheRechts();
	public void dreheRechtsGrad(int winkel);
	public void dreheRechtsZeit(long zeitInSek);
	public void dreheLinks();
	public void dreheLinksGrad(int winkel);
	public void dreheLinksZeit(long zeitInSek);
	public void stoppe();
}
