import lejos.nxt.*;

public class Fahrzeug implements IFahrzeugInterface, IAnzeigenInterface {
	public NXTRegulatedMotor rechterMotor = Motor.B;
	public NXTRegulatedMotor linkerMotor = Motor.C;
	final float DURCHMESSER_REIFEN = 5.6f;
	final float ABSTAND_ACHE = 11.1f;
	final int EINS_METER = 5715;
	final int geradeausR = 359;
	final int geradeausL = 361;

	public void setRechterMotor(NXTRegulatedMotor rechterMotor) {
		this.rechterMotor = rechterMotor;
	}

	public void setLinkerMotor(NXTRegulatedMotor linkerMotor) {
		this.linkerMotor = linkerMotor;
	}

	public void setGeschwindigkeit(int links, int rechts) {
		rechterMotor.setSpeed(rechts);
		linkerMotor.setSpeed(links);
	}

	Fahrzeug() {
		rechterMotor.setAcceleration(6000);
		linkerMotor.setAcceleration(6000);
		rechterMotor.setSpeed(geradeausR);
		linkerMotor.setSpeed(geradeausL);

	}

	Fahrzeug(int geschwindigkeitLinks, int geschwindigkeitRechts) {
		rechterMotor.setSpeed(geschwindigkeitRechts);
		linkerMotor.setSpeed(geschwindigkeitLinks);
		rechterMotor.setAcceleration(12000);
		linkerMotor.setAcceleration(12000);
	}

	public void fahreVorwaerts() {
		rechterMotor.forward();
		linkerMotor.forward();
	}

	public void fahreVorwaertsMitAcceleration() {

		rechterMotor.setAcceleration(150);
		linkerMotor.setAcceleration(150);
		fahreVorwaerts();
	}
	public void setAcceleration(int acc) {
		rechterMotor.setAcceleration(acc);
		linkerMotor.setAcceleration(acc);
	}

	public void fahreVorwaertsStrecke(int streckeInCM) {
		fahreVorwaerts();
		try {
			Thread.sleep(EINS_METER / 100 * streckeInCM /** 360 / getLinkerMotor()*/);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void fahreVorwaertsZeit(long zeitInSek) {
		fahreVorwaerts();
		try {
			Thread.sleep(zeitInSek*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void fahreRueckwaerts() {
		rechterMotor.backward();
		linkerMotor.backward();
	}

	public void fahreRueckwaertsStrecke(int streckeInCM) {
		fahreRueckwaerts();
		try {
			Thread.sleep((EINS_METER / 100) * streckeInCM * 360 / getMotorSpeed());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void fahreRueckwaertsZeit(long zeitInSek) {
		fahreRueckwaerts();
		try {
			Thread.sleep(1000 * zeitInSek);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void dreheRechts() {
		rechterMotor.backward();
		linkerMotor.forward();
	}

	public void dreheRechts2() {
		rechterMotor.forward();
	}
	
	public void dreheRechtsGrad(int winkel) {
		linkerMotor.rotate((int) (winkel*4.171)/2,true);
		rechterMotor.rotate(-(int) (winkel*4.171)/2,false);
	}
	
	public void dreheRechtsGrad2(int winkel) {
		rechterMotor.rotate((int) -(winkel*4.171), false);
	
	}

	public void dreheRechtsZeit(long zeitInSek) {
		dreheRechts();
		try {
			Thread.sleep(1000 * zeitInSek);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	
	public void dreheLinks() {
		rechterMotor.forward();
		linkerMotor.backward();
	}

	public void dreheLinks2() {
		linkerMotor.forward();
	}

	public void dreheLinksGrad(int winkel) {
		rechterMotor.rotate((int) (winkel*4.171)/2, true);
		linkerMotor.rotate((int) -(winkel*4.171)/2,false);
	}
	public void dreheLinksGrad2(int winkel) {
		rechterMotor.rotate((int) (winkel*4.171), true);
		
	}

	public void dreheLinksZeit(long zeitInSek) {
		dreheLinks();
		try {
			Thread.sleep(1000 * zeitInSek);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stoppe();
	}

	public void stoppe() {
		rechterMotor.stop();
		linkerMotor.stop(true);
	}

	public void zeigeText(String text) {
		LCD.clear();
		LCD.drawString(text, 1, 1);
	}

	public void zeigeZahl(float zahl) {
		LCD.clear();
		LCD.drawString(String.valueOf(zahl), 1, 1);
	}

	public int getRechterMotor() {
		return rechterMotor.getSpeed();
	}

	public int getLinkerMotor() {
		return linkerMotor.getSpeed();
	}

	public int getMotorSpeed() {
		return ((getRechterMotor() + getLinkerMotor()) / 2);
	}

	public float getDURCHMESSER_REIFEN() {
		return DURCHMESSER_REIFEN;
	}

	public float getABSTAND_ACHE() {
		return ABSTAND_ACHE;
	}
}