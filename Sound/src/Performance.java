import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.LCD;
import lejos.nxt.SensorPort;
import lejos.nxt.SoundSensor;

public class Performance {
	static int rechts = 358;
	static int links = 360;
	static IFahrzeugInterface einsFahrzeug = new Fahrzeug(links, rechts);
	static Thread saasThread = new SoundThread();
	static SoundSensor ohrDesTauben = new SoundSensor(SensorPort.S4);
	boolean lAUUUT = false;

	public static void main(String[] args) {
		int perf[] = { 410, 25, 1100, 0, 1125, 35, 1900, 0, 1945, 25, 2800, 0, 2825, 35, 3800, 0, 3845, -25, 4700, 0,
				4745, -35, 5700, 0, 5765, -25, 6500, 0, 6585, -35, 7600, 0, 7605, 1, 8600, 0, 8625, -1, 9600, 0, 9605,
				1, 10550, 0, 10585, 26, 14300, 0, 14345, 26, 18200, 0, 18285, 26, 22000, 0, 22045, 26, 25900, 0, 25985,
				36, 29800, 0, 29805, 36, 33700, 0, 33705, 36, 37400, 0, 37445, 36, 41300, 0, 41305, 12, 45000, 0 };
		int ix = 0;
		File genFile = new File("timingPoints.txt");
		DataOutputStream tmpGenOut = null;
		try {
			tmpGenOut = new DataOutputStream(new FileOutputStream(genFile));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		final DataOutputStream genOut = tmpGenOut;
		Button.ENTER.waitForPress();
		saasThread.start();
		// einsFahrzeug.fahreVorwaerts();
		/*
		 * for (int i = 10; i < 200; i++) { einsFahrzeug.setGeschwindigkeit(i, i); try {
		 * Thread.sleep(100); } catch (InterruptedException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); } }
		 */

		/*
		 * Button.RIGHT.addButtonListener(new ButtonListener() { public void
		 * buttonPressed(Button b) { if (genOut != null) { if
		 * (((SoundThread)saasThread).saasSound.time <
		 * ((SoundThread)saasThread).saasSound.score[((SoundThread)saasThread).saasSound
		 * .score.length - 4]) { try {
		 * genOut.writeChars(Integer.toString(((SoundThread)saasThread).saasSound.time)
		 * + "\n"); } catch (IOException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } } else { try { genOut.close(); } catch (IOException e)
		 * { // TODO Auto-generated catch block e.printStackTrace(); } } } }
		 * 
		 * @Override public void buttonReleased(Button b) {
		 * 
		 * } });
		 */
		/*
		 * public boolean isLaut() { if(ohrDesTauben.readValue()) return true; }
		 */
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while (((SoundThread) saasThread).saasSound.time < ((SoundThread) saasThread).saasSound.score[((SoundThread) saasThread).saasSound.score.length
				- 4]) {
			/*
			 * for(int i=0; i<999999;i++) { LCD.drawInt(ohrDesTauben.readValue(), 1, 5); try
			 * { Thread.sleep(100); } catch (InterruptedException e) { // TODO
			 * Auto-generated catch block e.printStackTrace(); }
			 */

			if (ix < perf.length) {
				if (perf[ix] <= ((SoundThread) saasThread).saasSound.time) {

					switch (perf[ix + 1]) {
					case 1:
						LCD.drawString("fahreVorwaerts", 4, 4);
						LCD.refresh();
						einsFahrzeug.fahreVorwaerts();
						break;
					case 12:
						LCD.drawString("beschleunige", 4, 4);
						LCD.refresh();

						einsFahrzeug.fahreVorwaertsMitAcceleration();
						einsFahrzeug.setGeschwindigkeit(361, 359);
						break;
					case 11:
						LCD.drawString("fahreVorwaerts", 4, 4);
						LCD.refresh();
						einsFahrzeug.setGeschwindigkeit(50, 50);
						einsFahrzeug.fahreVorwaerts();
						break;
					case 2:
						LCD.drawString("dreheLinks", 4, 4);
						LCD.refresh();
						einsFahrzeug.dreheLinks();
						break;
					case 25:
						LCD.drawString("dreheLinks", 4, 4);
						LCD.refresh();
						einsFahrzeug.setGeschwindigkeit(0, einsFahrzeug.getRechterMotor());
						einsFahrzeug.dreheLinks();
						break;
					case 26:
						LCD.drawString("dreheLinks", 4, 4);
						LCD.refresh();
						einsFahrzeug.dreheLinksGrad(90);
						einsFahrzeug.stoppe();
						einsFahrzeug.fahreVorwaerts();
						break;
					case -25:
						LCD.drawString("dreheLinksZurück", 4, 4);
						LCD.refresh();
						einsFahrzeug.setGeschwindigkeit(0, einsFahrzeug.getRechterMotor());
						IFahrzeugInterface.rechterMotor.backward();
						break;
					case 3:
						LCD.drawString("dreheRechts", 4, 4);
						LCD.refresh();
						einsFahrzeug.dreheRechts();
						break;
					case 35:
						LCD.drawString("dreheRechts", 4, 4);
						LCD.refresh();
						einsFahrzeug.setGeschwindigkeit(einsFahrzeug.getLinkerMotor(), 0);
						einsFahrzeug.dreheRechts();
						break;
					case 36:
						LCD.drawString("dreheRechtsG", 4, 4);
						LCD.refresh();
						einsFahrzeug.dreheRechtsGrad(90);
						einsFahrzeug.stoppe();
						einsFahrzeug.fahreRueckwaerts();
						break;
					case -35:
						LCD.drawString("dreheRechtsZurück", 4, 4);
						LCD.refresh();
						einsFahrzeug.setGeschwindigkeit(einsFahrzeug.getLinkerMotor(), 0);
						IFahrzeugInterface.linkerMotor.backward();
						break;
					case -1:
						LCD.drawString("fahreRueckwaerts", 4, 4);
						LCD.refresh();
						einsFahrzeug.fahreRueckwaerts();
						break;
					default:
						LCD.drawString("stoppe", 4, 4);
						LCD.refresh();
						einsFahrzeug.setAcceleration(12000);
						einsFahrzeug.setGeschwindigkeit(links, rechts);
						einsFahrzeug.stoppe();
						break;
					}

					ix += 2;
				}
			} else {
				break;
			}

			try {
				Thread.sleep(10);
			} catch (Exception e) {

			}
		}
		if (genOut != null) {
			try {
				genOut.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
