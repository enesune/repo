
public class Note {
	public int time = 0;
	public int freq = 0;
	public int length = 0;
	
	public Note() {
		
	}
	
	public Note(int time, int length, int freq) {
		this.time = time;
		this.length = length;
		this.freq = freq;
	}
	
}
