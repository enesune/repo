import lejos.nxt.Motor;
import lejos.nxt.NXTRegulatedMotor;

public interface IFahrzeugInterface {
	public NXTRegulatedMotor rechterMotor= Motor.B;
	public NXTRegulatedMotor linkerMotor = Motor.C;
	
	public void fahreVorwaertsMitAcceleration();
	public void setAcceleration(int acc);
	public float getDURCHMESSER_REIFEN();
	public float getABSTAND_ACHE();
	public int getLinkerMotor();
	public int getRechterMotor();
	public int getMotorSpeed();
	public void setGeschwindigkeit(int links, int rechts);
	public void fahreVorwaerts();	 
	public void fahreVorwaertsStrecke(int streckeInMetern);
	public void fahreVorwaertsZeit(long zeitInSek);
	public void fahreRueckwaerts();	
	public void fahreRueckwaertsStrecke(int streckeInMetern);
	public void fahreRueckwaertsZeit(long zeitInSek);
	public void dreheRechts();
	public void dreheRechts2();
	public void dreheRechtsGrad(int winkel);
	public void dreheRechtsGrad2(int winkel);
	public void dreheRechtsZeit(long zeitInSek);
	public void dreheLinks();
	public void dreheLinks2();
	public void dreheLinksGrad(int winkel);
	public void dreheLinksGrad2(int winkel);
	public void dreheLinksZeit(long zeitInSek);
	public void stoppe();
}
